#!/usr/bin/env python3

import os, sys
import subprocess
import io
from directories import parseandSet,dirImport,cd

#set vars based on parsed args
modname=dirImport
packVer=parseandSet.packVer
ads=parseandSet.ads
debugMode=parseandSet.debugMode
appName=parseandSet.appName
platform=parseandSet.platform
debug=parseandSet.debug
release=parseandSet.release
verbosity=parseandSet.verbosity
ads=parseandSet.ads
debugMode=parseandSet.debugMode
packVer=parseandSet.packVer
rokuIP=parseandSet.rokuIP
bundleStr=parseandSet.bundleStr
gFlags=parseandSet.gFlags
bFlags=parseandSet.bFlags
linuxPath=parseandSet.linuxPath
stackName=parseandSet.stackName
appDef=parseandSet.appDef
stage=parseandSet.stage
region=parseandSet.region
reactSamples=parseandSet.reactSamples
reactTestApps=parseandSet.reactTestApps
cloudApps=parseandSet.cloudApps
reactApps=parseandSet.reactApps
supportedApps= parseandSet.supportedApps
uswishDir=dirImport.uswishDir
packageDir=dirImport.packageDir
isHeadless=parseandSet.isHeadless
isInteractive=parseandSet.isInteractive
        
#generate our app with either uswish unversioned or packaged YouiEngine, BUILD_TESTER_APPS will be ignored when not needed this is fine
def generate():
    print("Generating...")
    if(parseandSet.args.packVer):
        with cd(getattr(modname,appName+"YouiDir")):
                os.system("./generate.rb -p "+platform+""+bundleStr+" -c "+debugMode+" -d YI_BUILD_CLOUD_SERVER=ON -d YI_BUILD_TESTER_APPS=ON "+gFlags +verbosity )
    elif (parseandSet.args.repo):
        with cd(dirImport.uswishDir):
                  os.system("./generate.rb -p "+platform+""+bundleStr+" -c "+debugMode+" -d YI_BUILD_CLOUD_SERVER=ON -d YI_BUILD_TESTER_APPS=ON "+gFlags +verbosity )

#build our app and its sub directories
def buildApp():
    print("Building...")
    if (parseandSet.args.repo):
        with cd(dirImport.uswishDir):
            os.system("./build.rb -b allinone/"+platform+"/"+linuxPath+" -c "+debugMode+" -t " +appName +verbosity )
    elif (parseandSet.args.packVer):
        with cd(getattr(modname,appName+"YouiDir")):
            os.system("./build.rb -b build/"+platform+"/"+linuxPath+" -c "+debugMode+" -t " +appName +verbosity )

#Yarn server for RN Apps
def startYarn():
    if (reactApps.__contains__(appName)):
        print("starting Yarn...")
        if (parseandSet.args.repo):
            with cd(getattr(modname,"yarn"+appName+"Repo")):
                subprocess.call("yarn",stdout=open(os.devnull, "w"))
                subprocess.Popen(["yarn" , "start"],stdout=open(os.devnull, "w"))
        elif (parseandSet.args.packVer):
            with cd(getattr(modname,"yarn"+appName+"Pack")):
                subprocess.call("yarn",stdout=open(os.devnull, "w"))
                subprocess.Popen(["yarn" , "start"],stdout=open(os.devnull, "w"))

#start app local server
def startServer():
    if(not parseandSet.args.stack):
            print("Starting local server...")
            if (parseandSet.args.repo):
                with cd(getattr(modname,"server"+appName+"Repo"+"_"+platform)):
                    os.system("./" +appName+isHeadless+isInteractive)
            elif (parseandSet.args.packVer):
                with cd(getattr(modname,"server"+appName+"Pack"+"_"+platform)):
                    os.system("./" +appName+isHeadless+isInteractive)

#deploy the client to Roku Device and check if it needs to be built for a stack deployment
def buildClient():
    print("Building client...")
    if (parseandSet.args.repo):
        with cd(getattr(modname,"client"+appName+"Repo")):
            if(parseandSet.args.stack):
                os.system("./build.rb -s "+stage+"."+stackName+".youi-cloud-test.com:80 -r "+rokuIP+ " -g "+bFlags +verbosity )
            else:
                 os.system("./build.rb -l -r "+rokuIP+ " -g "+bFlags)
    elif (parseandSet.args.packVer):
        with cd(getattr(modname,"client"+appName+"Pack")):
            if(parseandSet.args.stack):
                os.system("./build.rb -e "+packageDir+packVer+" -s "+stage+"."+stackName+".youi-cloud-test.com:80 -r "+rokuIP+ " -g "+bFlags +verbosity )
            else:
                os.system("./build.rb -e "+packageDir+packVer+" -l -r "+rokuIP+ " -g "+bFlags +verbosity )

#deploy server bundle to the stack            
def deployToStack():
    if(parseandSet.args.stack):
        print("Deploying to stack...")
        if (parseandSet.args.repo):
            with cd(getattr(modname,"server"+appName+"Repo"+"_"+platform)):
                os.system("./deploy.sh "+appDef+" "+appName+" "+stackName+" "+stage+" "+stackName+" "+region )
        elif (parseandSet.args.packVer):
            with cd(getattr(modname,"server"+appName+"Pack"+"_"+platform)):
                os.system("./deploy.sh "+appDef+" "+appName+" "+stackName+" "+stage+" "+stackName+"  "+region)
    

#kill any trailing node processes before starting yarn as best practice
def killNode():
    print("kill node instances")
    os.system("killall -9 node")

if __name__ == "__main__":
    parseandSet()
    killNode()
    startYarn()
    generate()
    buildApp()
    buildClient()
    startServer()
    deployToStack()

