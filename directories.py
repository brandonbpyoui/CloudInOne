#!/usr/bin/env python3

import os, sys
import argparse
import subprocess
import io


#Context manager for changing the relative working directory
class cd:
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)
        
class parseandSet:
    #ARGUMENTS/FLAGS
    parser = argparse.ArgumentParser(description='CloudInOne')
    parser.add_argument('-t','--targetApp', help='The Youi target app name', required=True, dest='targetApp')
    parser.add_argument('-rep','--repo', help='stating we are using a repo', required=False, action='store_true', dest='repo')
    parser.add_argument('-pv','--packver', help='stating we are using a package', required=False, dest='packVer')
    parser.add_argument('-ads','--ads', help='utilize the clientWithAds', required=False, action='store_true', dest='ads')
    parser.add_argument('-p','--platform', help='what OS we are using', required=True, dest='platform')
    parser.add_argument('-rel','--release', help='this is to specify we are building as release ', required=False, action='store_true', dest='release')
    parser.add_argument('-deb','--debug', help='this is to specify we are building as debug ', required=False, action='store_true', dest='debug')
    parser.add_argument('-n1','--headless', help='use the null-renderer for performance like on the stack ', required=False, action='store_true', dest='headless')
    parser.add_argument('-i','--interactive', help='use interactive mode ', required=False, action='store_true', dest='interactive')
    parser.add_argument('-rIP','--rokuIP', help='we are deploying the client on a Roku with its IP ', required=False, dest='rokuIP')
    parser.add_argument('-gFlags','--genFlags', help='generate flag "-d"', required=False, dest='gFlags')
    parser.add_argument('-bFlags','--buildFlags', help='compile flag "-c"', required=False, dest='bFlags')
    parser.add_argument('-bun','--bundle', help='bundle with yarn?', required=False, dest='bundle', action='store_true')
    parser.add_argument('-stk','--stack', help='Name of the stack to deploy to. Bundle, and deploy to this stack name', required=False, dest='stack')
    parser.add_argument('-stg','--stage', help='eg.v101e1', required=False, dest='stage')
    parser.add_argument('-reg','--region', help='eg: us-east-1', required=False, dest='region')
    parser.add_argument('-v','--verbose', help='show text from stages of deployment', required=False, action='store_true', dest='verbose')
    args = parser.parse_args()
    
    
    #custom dirs for other users
    uswishDir= "~/git/uswish/"
    packageDir= "~/youiengine/"
    
    
    # App name and build config
    appName=args.targetApp
    platform=args.platform
    debug=args.debug
    release=args.release
    verbosity=" >/dev/null "

    #default app config
    ads="client"
    debugMode="Debug"
    packVer=""
    rokuIP="NO ROKU IP"
    isHeadless=""
    isInteractive=""

    #build overrides
    bundleStr=""
    gFlags=""
    bFlags=""
    linuxPath=""

    #default stack vars
    stackName=""
    appDef="youiapp"
    stage="v101e1"
    region="default us-east-1"

    #supported apps
    reactSamples=["RNSampleApp"]
    reactTestApps=["RNTesterApp"]
    cloudApps=["CloudTester","CloudVideoPlayer"]
    reactApps=reactSamples+reactTestApps
    supportedApps= reactApps+cloudApps

    #Stack Overrides
    if(args.stack):
        stackName=args.stack
    if(args.stage):
        stage=args.stage
    if(args.region):
        region="default "+args.region

    #build/gen additional flags?
    if(args.gFlags):
        gFlags=args.gFlags

    if(args.bFlags):
        gFlags=args.gFlags

    #package?
    if(args.packVer):
        packVer=args.packVer
    #roku?
    if(args.rokuIP):
        rokuIP=args.rokuIP

    #Determine version path
    if(args.release):
        debugMode="Release"
    
    #use null-renderer for performance like on the stack
    if(args.headless):
        isHeadless=" -n1"
    #use null-renderer for performance like on the stack
    if(args.interactive):
        isInteractive=" -i"
    
    #linux?
    if(args.platform == "linux"):
        linuxPath=debugMode

    #client with ads?
    if(args.ads):
        ads="clientWithClientSideAds"
    if(args.verbose):
        verbosity=""

    assert (supportedApps.__contains__(appName)),"Not a supported Application name"
    assert (platform == "osx" or platform == "linux"),"Not a supported platform for Cloud"

    #Bundle RN apps?
    if(args.bundle or args.stack):
            if (reactSamples.__contains__(appName)):
                if(args.repo):
                    bundleStr=" --local --file index.youi.js --working_directory "+uswishDir+"react/samples/"+appName
                else:
                    bundleStr=" --local --file index.youi.js"
            elif (appName == "RNTesterApp"):
                if(args.repo):
                    bundleStr=" --local --directory tests --working_directory "+uswishDir+"react/test/"+appName
                else:
                    bundleStr=" --local --directory tests"

class dirImport:
        #use args to set directories
        packVer=parseandSet.packVer
        ads= parseandSet.ads
        debugMode=parseandSet.debugMode
        
        ####################################################################################################
        
        #custom dirs for other users
        uswishDir=parseandSet.uswishDir
        packageDir=parseandSet.packageDir

        #APP DIR
        RNSampleAppYouiDir= packageDir+packVer+"/samples/RNSampleApp/youi"
        CloudTesterYouiDir= packageDir+packVer+"/testApps/apps/CloudTester"
        RNTesterAppYouiDir= packageDir+packVer+"/testApps/RNTesterApp/youi"
        #YARN DIR
        yarnRNTesterAppRepo= uswishDir+"react/test/RNTesterApp"
        yarnRNTesterAppPack=  packageDir+packVer+"/testApps/RNTesterApp"
        yarnRNSampleAppRepo= uswishDir+"react/samples/RNSampleApp"
        yarnRNSampleAppPack=  packageDir+packVer+"/samples/RNSampleApp"
        #CLIENT DIR
        clientCloudTesterRepo= uswishDir+"cloud/Test/apps/CloudTester/"+ads
        clientCloudTesterPack=  packageDir+packVer+"/testApps/apps/CloudTester/"+ads
        clientRNTesterAppRepo= uswishDir+"react/test/RNTesterApp/client"
        clientRNTesterAppPack=  packageDir+packVer+"/testApps/RNTesterApp/client"
        clientRNSampleAppRepo= uswishDir+"react/samples/RNSampleApp/client"
        clientRNSampleAppPack=  packageDir+packVer+"/samples/RNSampleApp/client"
        clientCloudVideoPlayerRepo= uswishDir+"cloud/samples/CloudVideoPlayer/"+ads
        clientAurynPack= uswishDir+"cloud/client/rokuclient"
        ############################ CLOUD SERVER DIRS OSX #################################################
        #SERVER DIR
        serverCloudTesterRepo_osx= uswishDir+"allinone/osx/cloud/Test/apps/CloudTester/"+debugMode
        serverCloudTesterPack_osx=  packageDir+packVer+"/testApps/apps/CloudTester/build/osx/"+debugMode
        serverCloudVideoPlayerRepo_osx= uswishDir+"allinone/osx/cloud/samples/CloudVideoPlayer/"+debugMode
        serverRNTesterAppRepo_osx= uswishDir+"allinone/osx/react/test/RNTesterApp/youi/"+debugMode
        serverRNTesterAppPack_osx=  packageDir+packVer+"/testApps/RNTesterApp/youi/build/osx/"+debugMode
        serverRNSampleAppRepo_osx= uswishDir+"allinone/osx/react/samples/RNSampleApp/youi/"+debugMode
        serverRNSampleAppPack_osx=  packageDir+packVer+"/samples/RNSampleApp/youi/build/osx/"+debugMode
        ############################ CLOUD SERVER DIRS LINUX ################################################
        #SERVER DIR
        serverCloudTesterRepo_linux= uswishDir+"allinone/linux/"+debugMode+"/cloud/Test/apps/CloudTester/bin"
        serverCloudTesterPack_linux=  packageDir+packVer+"/testApps/apps/CloudTester/build/linux/"+debugMode+"/bin"
        serverCloudVideoPlayerRepo_linux= uswishDir+"allinone/linux/"+debugMode+"/cloud/samples/CloudVideoPlayer/bin"
        serverRNTesterAppRepo_linux= uswishDir+"allinone/linux/"+debugMode+"/react/test/RNTesterApp/youi/bin"
        serverRNTesterAppPack_linux=  packageDir+packVer+"/testApps/RNTesterApp/youi/build/linux/"+debugMode+"/bin"
        serverRNSampleAppRepo_linux= uswishDir+"allinone/linux/"+debugMode+"/react/samples/RNSampleApp/youi/bin"
        serverRNSampleAppPack_linux=  packageDir+packVer+"/samples/RNSampleApp/youi/build/linux/"+debugMode+"/bin"
        #######################################################################################################


