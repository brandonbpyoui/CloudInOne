 ### CloudInOne
 
 This script will allow you to build all configuration options for Cloud: packages, remotes, debug, release, bundled/non-bundled and deploy to a Roku Device by IP
 
(as listed here https://youilabs.atlassian.net/wiki/spaces/PQ/pages/753664032/Cloud+Repositories+Packages)

Requirements: Python 3 installation

### Instructions:
- git clone the repo to any directory of your choosing
- make sure the files have permission to run on your system (you can use: "sudo chmod +rwx" followed by the file name(s) )
- example use case : ./cloudinone.py -deb -rep -p osx -t RNTesterApp -rIP 10.100.88.202

### All Arguments:
```
-h, --help                                show this help message and exit
-t , --targetApp                          The Youi.TV target test app name
-rep, --repo                              stating we are using a repository (remote)
-pv , --packver                           stating we are using a specific package
-ads, --ads                               utilize the clientWithAds
-p , --platform                           what OS we are using (Linux and OS X only)
-rel, --release                           this is to specify we are building as release
-deb, --debug                             this is to specify we are building as debug
-rIP , --rokuIP                           we are deploying the client on a Roku with its IP
-gFlags , --genFlags                      generate flag override usage: -gFlags "-d YI_BUILD_CLOUD_SERVER=OFF"
-bFlags , --buildFlags                    build flag override usage: -bFlags "-c YI_VARIABLE"
-bun, --bundle                            bundle app with yarn?
-stk , --stack                            Name of a current stack in EC2 to deploy to ie "qastack"
-stg , --stage                            stage of the stack you are going to deploy to ie: "v101e1" 
-reg REGION, --region                     eg: us-east-1
-v, --verbose                             Show all logs as the app is building and no longer hides them
```


### Arguments that need need params:
```
-t , --targetApp                         The Youi.TV target test app name
-pv , --packver                          stating we are using a specific package
-stk , --stack                           Name of a current stack in EC2 to deploy to ie "qastack"
-stg , --stage                           stage of the stack you are going to deploy to ie: "v101e1" 
-reg , --region                          eg: us-east-1
-rIP , --rokuIP                          we are deploying the client on a Roku with its IP
-gFlags , --genFlags                     generate flag override usage: -gFlags "-d YI_BUILD_CLOUD_SERVER=OFF"
-bFlags , --buildFlags                   build flag override usage: -bFlags "-c YI_VARIABLE"
```

### Arguments that function as flags with no params required:
```
-h, --help                               show this help message and exit
-rep, --repo                             stating we are using a repository (remote)
-rel, --release                          this is to specify we are building as release
-deb, --debug                            this is to specify we are building as debug
-bun, --bundle                           bundle app with yarn?
-ads, --ads                              utilize the clientWithAds where applicable
-v, --verbose                            Show all logs as the app is building and no longer hides them
```
