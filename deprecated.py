#!/usr/bin/env python3

import io
import os, sys
import argparse
import subprocess


#Context manager for changing the relative working directory
class cd:
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


#Kill any trailing node instances (yarn may fail later if not)
with cd("~"):
    os.system("killall -9 node")

#retrieve the module name we are in
modname=sys.modules[__name__]

#ARGUMENTS/FLAGS
parser = argparse.ArgumentParser(description='Youi Cloud Deploy')
parser.add_argument('-t','--targetApp', help='The Youi target app name', required=True, dest='targetApp')
parser.add_argument('-rep','--repo', help='stating we are using a repo', required=False, action='store_true', dest='repo')
parser.add_argument('-pv','--packver', help='stating we are using a package', required=False, dest='packVer')
parser.add_argument('-ads','--ads', help='utilize the clientWithAds', required=False, action='store_true', dest='ads')
parser.add_argument('-p','--platform', help='what OS we are using', required=True, dest='platform')
parser.add_argument('-rel','--release', help='this is to specify we are building as release ', required=False, action='store_true', dest='release')
parser.add_argument('-deb','--debug', help='this is to specify we are building as debug ', required=False, action='store_true', dest='debug')
parser.add_argument('-rIP','--rokuIP', help='we are deploying the client on a Roku with its IP ', required=False, dest='rokuIP')
parser.add_argument('-gFlags','--genFlags', help='generate flag "-d"', required=False, dest='gFlags')
parser.add_argument('-bFlags','--buildFlags', help='generate flag "-d"', required=False, dest='bFlags')
parser.add_argument('-bun','--bundle', help='bundle with yarn?', required=False, dest='bundle', action='store_true')
parser.add_argument('-stack','--stack', help='bundle, and deploy to stack', required=False, dest='stack')
args = parser.parse_args()



# Vars
appName=args.targetApp
platform=args.platform
debug=args.debug
release=args.release
ads="client"
debugMode="Debug"
packVer=""
rokuIP="NO ROKU IP"
bundleStr=""
gFlags=""
bFlags=""
linuxPath=""
stackName=""
stage="v101e1"
region="default us-east-1"
reactSamples=["RNSampleApp","Auryn"]
cloudApps=["CloudTester","CloudVideoPlayer"]
reactApps=["RNSampleApp","Auryn","YiReactNativeTestApp"]
supportedApps= reactApps+cloudApps

assert (supportedApps.__contains__(appName)),"Not a supported Appplication name"

#stackname
if(args.stack):
    stackName=args.stack

#optional flags?
if(args.gFlags):
    gFlags=args.gFlags

if(args.bFlags):
    gFlags=args.gFlags

#package?
if(args.packVer):
    packVer=args.packVer
#roku?
if(args.rokuIP):
   rokuIP=args.rokuIP

#Determine version path
if(args.release):
    debugMode="Release"

#linux?
if(args.platform == "linux"):
    linuxPath=debugMode

#client with ads?
if(args.ads):
    ads="clientWithClientSideAds"


#Bundle RN apps?
if(args.bundle or args.stack):
    if (reactSamples.__contains__(appName)):
        bundleStr=" --local --file index.youi.js --working_directory `pwd`/react/RNSampleApp"

    elif (appName == "YiReactNativeTestApp"):
        bundleStr=" --local --directory tests --working_directory `pwd`/react/YiReactNativeTestApp "

####################################################################################################
#APP DIR
uswishDir= "~/git/uswish"
RNSampleAppYouiDir="~/youiengine/"+packVer+"/samples/RNSampleApp/youi"
CloudTesterYouiDir="~/youiengine/"+packVer+"/testApps/apps/CloudTester"
YiReactNativeTestAppYouiDir="~/youiengine/"+packVer+"/testApps/YiReactNativeTestApp/youi"
AurynYouiDir="~/git/Auryn/youi"

#YARN DIR
yarnYiReactNativeTestAppRepo= "~/git/uswish/react/YiReactNativeTestApp"
yarnYiReactNativeTestAppPack= "~/youiengine/"+packVer+"/testApps/YiReactNativeTestApp"
yarnRNSampleAppRepo= "~/git/uswish/react/RNSampleApp"
yarnRNSampleAppPack= "~/youiengine/"+packVer+"/samples/RNSampleApp"
yarnAurynPack= "~/git/Auryn/"

#CLIENT DIR
clientCloudTesterRepo= "~/git/uswish/cloud/Test/apps/CloudTester/"+ads
clientCloudTesterPack= "~/youiengine/"+packVer+"/testApps/apps/CloudTester/"+ads
clientYiReactNativeTestAppRepo= "~/git/uswish/react/YiReactNativeTestApp/client"
clientYiReactNativeTestAppPack= "~/youiengine/"+packVer+"/testApps/YiReactNativeTestApp/client"
clientRNSampleAppRepo= "~/git/uswish/react/RNSampleApp/client"
clientRNSampleAppPack= "~/youiengine/"+packVer+"/samples/RNSampleApp/client"
clientCloudVideoPlayerRepo= "~/git/uswish/cloud/samples/CloudVideoPlayer/"+ads
clientAurynPack= "~/git/uswish/react/RNSampleApp/client"

############################ CLOUD SERVER DIRS OSX #################################################

#SERVER DIR
serverCloudTesterRepo_osx= "~/git/uswish/allinone/osx/cloud/Test/apps/CloudTester/"+debugMode
serverCloudTesterPack_osx= "~/youiengine/"+packVer+"/testApps/apps/CloudTester/build/osx/"+debugMode
serverCloudVideoPlayerRepo_osx= "~/git/uswish/allinone/osx/cloud/samples/CloudVideoPlayer/"+debugMode
serverYiReactNativeTestAppRepo_osx= "~/git/uswish/allinone/osx/react/YiReactNativeTestApp/youi/"+debugMode
serverYiReactNativeTestAppPack_osx= "~/youiengine/"+packVer+"/testApps/YiReactNativeTestApp/youi/build/osx/"+debugMode
serverRNSampleAppRepo_osx= "~/git/uswish/allinone/osx/react/RNSampleApp/youi/"+debugMode
serverRNSampleAppPack_osx= "~/youiengine/"+packVer+"/samples/RNSampleApp/youi/build/osx/"+debugMode
serverAurynPack_osx="~/git/Auryn/youi/build/osx/"+debugMode
############################ CLOUD SERVER DIRS LINUX ################################################

#SERVER DIR
serverCloudTesterRepo_linux= "~/git/uswish/allinone/linux/"+debugMode+"/cloud/Test/apps/CloudTester/bin"
serverCloudTesterPack_linux= "~/youiengine/"+packVer+"/testApps/apps/CloudTester/build/linux/"+debugMode+"/bin"
serverCloudVideoPlayerRepo_linux= "~/git/uswish/allinone/linux/cloud/samples/CloudVideoPlayer/"+debugMode+"/bin"
serverYiReactNativeTestAppRepo_linux= "~/git/uswish/allinone/linux/"+debugMode+"/react/YiReactNativeTestApp/youi/bin"
serverYiReactNativeTestAppPack_linux= "~/youiengine/"+packVer+"/testApps/YiReactNativeTestApp/youi/build/linux/"+debugMode+"/bin"
serverRNSampleAppRepo_linux= "~/git/uswish/allinone/linux/"+debugMode+"/react/RNSampleApp/youi/bin"
serverRNsampleAppPack_linux= "~/youiengine/"+packVer+"/samples/RNSampleApp/youi/"+debugMode

#######################################################################################################



def generate():
    if(args.packVer):
        with cd(getattr(modname,appName+"YouiDir")):
            if (args.debug):
                os.system("./generate.rb -p "+platform+""+bundleStr+" -d YI_BUILD_CLOUD_SERVER=ON -d YI_BUILD_TESTER_APPS=ON "+gFlags)
            elif (args.release):
                os.system("./generate.rb -p "+platform+" -c Release"+bundleStr+" -d YI_BUILD_CLOUD_SERVER=ON "+gFlags)

    elif (args.repo):
        with cd(uswishDir):
            if (args.debug):
                  os.system("./generate.rb -p "+platform+""+bundleStr+" -d YI_BUILD_CLOUD_SERVER=ON -d YI_BUILD_TESTER_APPS=ON "+gFlags)
            elif (args.release):
                    os.system("./generate.rb -p "+platform+" -c Release"+bundleStr+" -d YI_BUILD_CLOUD_SERVER=ON "+gFlags)

def buildApp():
    if (args.repo):
        with cd(uswishDir):
            os.system("./build.rb -b allinone/"+platform+"/"+linuxPath+" -t " +appName)
    elif (args.packVer):
        with cd(getattr(modname,appName+"YouiDir")):
            os.system("./build.rb -b build/"+platform+"/"+linuxPath+" -t " +appName)

#Yarn server for RN Apps
def startYarn():
    if (reactApps.__contains__(appName)):
        if (args.repo):
            with cd(getattr(modname,"yarn"+appName+"Repo")):
                subprocess.call("yarn")
                subprocess.Popen(["yarn" , "start"])
        elif (args.packVer):
            with cd(getattr(modname,"yarn"+appName+"Pack")):
                subprocess.call("yarn")
                subprocess.Popen(["yarn" , "start"])

#start app local server
def startServer():
    if(not args.stack):
            if (args.repo):
                with cd(getattr(modname,"server"+appName+"Repo"+"_"+platform)):
                    os.system("./" +appName)
            elif (args.packVer):
                with cd(getattr(modname,"server"+appName+"Pack"+"_"+platform)):
                    os.system("./" +appName+ " -n1")

#deploy the client to Roku Device and check if it needs to be built for a stack deployment
def buildClient():
    if (args.repo):
        with cd(getattr(modname,"client"+appName+"Repo")):
            if(args.stack):
                os.system("./build.rb -s "+stage+"."+stackName+".youi-cloud-test.com:80 -r "+rokuIP+ " -g "+bFlags)
            else:
                 os.system("./build.rb -l -r "+rokuIP+ " -g "+bFlags)
    elif (args.packVer):
        with cd(getattr(modname,"client"+appName+"Pack")):
            if(args.stack):
                os.system("./build.rb -e ~/youiengine/"+packVer+" -s "+stage+"."+stackName+".youi-cloud-test.com:80 -r "+rokuIP+ " -g "+bFlags)
            else:
                os.system("./build.rb -e ~/youiengine/"+packVer+" -l -r "+rokuIP+ " -g "+bFlags)

#deploy server bundle to the stack            
def deployToStack():
    if(args.stack):
        if (args.repo):
            with cd(getattr(modname,"server"+appName+"Repo"+"_"+platform)):
                os.system("./deploy.sh youiapp "+appName+" "+stackName+" "+stage+" "+stackName+" "+region)
        elif (args.packVer):
            with cd(getattr(modname,"server"+appName+"Pack"+"_"+platform)):
                os.system("./deploy.sh youiapp "+appName+" "+stackName+" "+stage+" "+stackName+"  "+region)
    

if __name__ == "__main__":
    startYarn()
    generate()
    buildApp()
    buildClient()
    startServer()
    deployToStack()

